#!/bin/sh
#Author: Jordi van Nistelrooij - Webs en systems
#Website: https://websensystems.nl 
#Contact: info@websensystems.nl
#Date: 2019-09-18
#Version: 1.0
#Discription: Script for leaning userbased bayes with mailbox folders other then default (ham) and know spam folders (spam). Also use mbox spam from artinvoice

LOGPATH='/var/log/sa-learn/' 
mkdir -p $LOGPATH

spamfile=spam--`date '+%Y-%m-%d'`.gz
spamfile_unpacked=spam--`date '+%Y-%m-%d'`
wget -qN http://artinvoice.hu/spams/$spamfile
gunzip $spamfile

for USER in `ls /usr/local/directadmin/data/users`;
do
#rm $LOGPATH$USER.log -f
echo "=== Start ===" >> $LOGPATH$USER.log
echo $(date +%F) >> $LOGPATH$USER.log
echo "=============" >> $LOGPATH$USER.log
echo -e "Domain\tMailbox\tLearn\tFolder\tResult" >> $LOGPATH$USER.log
echo -e "===\t===\t===\t===\t===" >> $LOGPATH$USER.log



for DOMAINS in `ls -d /home/$USER/imap/*`;
do

if [ `find $DOMAINS -maxdepth 1 -type d | wc -l` -ge 2 ]
then
for MAILBOX in `ls -d $DOMAINS/*`;
do

if [ `find $MAILBOX -maxdepth 1 -type d | wc -l` -ge 2 ]
then
IFS=$'\n'
for HAMMAILBOXFOLDER in `ls -d $MAILBOX/Maildir/.* | egrep -v -i 'spam|Ongewenst|verwijderde|trash|drafts|concepten|junk|prullenbak|unwanted|deleted|\.$|\.\.$'`;
do
if [ -d "${HAMMAILBOXFOLDER}/cur" ]
then
echo -e `basename ${DOMAINS}` "\t" `basename ${MAILBOX}` "\tHAM\t" `basename ${HAMMAILBOXFOLDER}` "\t" `sa-learn --ham --db /home/$USER/.spamassassin "${HAMMAILBOXFOLDER}/cur"` >> $LOGPATH$USER.log
fi
done

IFS=$'\n'
for SPAMMAILBOXFOLDER in `ls -d $MAILBOX/Maildir/.* | egrep -i 'spam|Ongewenst|junk|unwanted'`;
do
if [ -d "${SPAMMAILBOXFOLDER}/cur" ]
then
echo -e `basename ${DOMAINS}` "\t" `basename ${MAILBOX}` "\tSPAM\t" `basename ${SPAMMAILBOXFOLDER}` "\t" `sa-learn --spam --db /home/$USER/.spamassassin "${SPAMMAILBOXFOLDER}/cur"` >> $LOGPATH$USER.log
fi
done
fi
done
fi
done
echo -e "all\tall\tSPAM\tartinvoice.hu" "\t" `sa-learn --spam --dbpath /home/$USER/.spamassassin --mbox $spamfile_unpacked` >> $LOGPATH$USER.log
echo -e "===\t===\t===\t===\t===" >> $LOGPATH$USER.log
echo "Syncing" >> $LOGPATH$USER.log
sa-learn --sync --dbpath /home/$USER/.spamassassin >> $LOGPATH$USER.log
echo "Bayes info" >> $LOGPATH$USER.log
sa-learn --dump magic --dbpath /home/$USER/.spamassassin >> $LOGPATH$USER.log
chown $USER:$USER /home/$USER/.spamassassin/bayes_*
echo "=== Done ===" >> $LOGPATH$USER.log
done

rm -f $spamfile
rm -fr $spamfile_unpacked

exit
